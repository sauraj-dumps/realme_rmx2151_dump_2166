## aosp_RMX2151-userdebug 12 SP1A.210812.016 eng.kano.20211008.165538 test-keys
- Manufacturer: realme
- Platform: 
- Codename: RMX2151
- Brand: realme
- Flavor: aosp_RMX2151-userdebug
- Release Version: 12
- Id: SP1A.210812.016
- Incremental: eng.kano.20211008.165538
- Tags: test-keys
- CPU Abilist: 
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: realme/aosp_RMX2151/RMX2151:12/SP1A.210812.016/kano10081655:userdebug/test-keys
- OTA version: 
- Branch: aosp_RMX2151-userdebug-12-SP1A.210812.016-eng.kano.20211008.165538-test-keys
- Repo: realme_rmx2151_dump_2166


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
